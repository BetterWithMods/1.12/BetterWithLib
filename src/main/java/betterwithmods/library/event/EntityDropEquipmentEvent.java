package betterwithmods.library.event;

import net.minecraft.entity.Entity;
import net.minecraftforge.event.entity.EntityEvent;

public class EntityDropEquipmentEvent extends EntityEvent {

    private final boolean wasRecentlyHit;
    private final int lootingModifier;

    public EntityDropEquipmentEvent(Entity entity, boolean wasRecentlyHit, int lootingModifier) {
        super(entity);
        this.wasRecentlyHit = wasRecentlyHit;
        this.lootingModifier = lootingModifier;
    }

    public boolean wasRecentlyHit() {
        return wasRecentlyHit;
    }

    public int getLootingModifier() {
        return lootingModifier;
    }
}

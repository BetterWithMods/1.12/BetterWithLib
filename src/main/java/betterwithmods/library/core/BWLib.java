package betterwithmods.library.core;

import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
@Mod(modid = BWLib.MODID, name = BWLib.NAME, version = BWLib.VERSION)
public class BWLib {
    public static final String MODID = "betterwithlib";
    public static final String NAME = "Better With Lib";
    public static final String VERSION = "${version}";

    public static Logger LOG = LogManager.getLogger(MODID);

    public static void log(String str) {
        LOG.info("[{} ASM] {}", MODID, str);
    }

    public static String[] STRUCTURECOMPONENT_SETBLOCKSTATE = new String[]{"setBlockState", "func_175811_a", "a", "(Lnet/minecraft/world/World;Lnet/minecraft/block/state/IBlockState;IIILnet/minecraft/world/gen/structure/StructureBoundingBox;)V"};

    public static String[] ENTITYLIVING_SETEQUIPMENTBASEDONDIFFICULTY  = new String[]{"setEquipmentBasedOnDifficulty", "func_180481_a", "a", "(Lnet/minecraft/world/DifficultyInstance;)V"};

    public static String[] ENTITYLIVING_DROPEQUIPMENT = new String[]{"dropEquipment","func_82160_b", "a" ,"(ZI)V"};
}

package betterwithmods.library.core.entity;

import betterwithmods.library.core.*;
import org.apache.commons.lang3.tuple.Pair;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

public class EquipmentDropTransformer implements Transformer {
    @Override
    public byte[] transform(byte[] basicClass) {
        BWLib.log("Transforming dropEquipment");
        MethodSignature sig = new MethodSignature(BWLib.ENTITYLIVING_DROPEQUIPMENT);

        return transform(basicClass, (Pair<MethodSignature, ClassTransformer.MethodAction>) Pair.of(sig, ClassTransformer.combine(
                (AbstractInsnNode node) -> { // Filter
                    return true;
                },
                (MethodNode method, AbstractInsnNode node) -> { // Action
                    InsnList newInstructions = new InsnList();

                    LabelNode ifSkipReturn = new LabelNode();

                    newInstructions.add(new VarInsnNode(Opcodes.ALOAD, 0));
                    newInstructions.add(new VarInsnNode(Opcodes.ILOAD, 1));
                    newInstructions.add(new VarInsnNode(Opcodes.ILOAD, 2));
                    newInstructions.add(new MethodInsnNode(Opcodes.INVOKESTATIC, ASMHooks.HOOKS, "onEntityDropEquipment", "(Lnet/minecraft/entity/Entity;ZI)Z", false));
                    newInstructions.add(new JumpInsnNode(Opcodes.IFEQ, ifSkipReturn));
                    newInstructions.add(new LabelNode());
                    newInstructions.add(new InsnNode(Opcodes.RETURN));
                    newInstructions.add(ifSkipReturn);

                    method.instructions.insertBefore(node, newInstructions);
                    return true;
                })));
    }

    @Override
    public String[] getClasses() {
        return new String[]{"net.minecraft.entity.EntityLiving"};
    }
}
